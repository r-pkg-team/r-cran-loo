Source: r-cran-loo
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Andreas Tille <tille@debian.org>
Section: gnu-r
Testsuite: autopkgtest-pkg-r
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-r,
               r-base-dev,
               r-cran-checkmate,
               r-cran-matrixstats,
               r-cran-posterior (>= 1.5.0)
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-cran-loo
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-cran-loo.git
Homepage: https://cran.r-project.org/package=loo
Rules-Requires-Root: no

Package: r-cran-loo
Architecture: all
Depends: ${R:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: GNU R leave-one-out cross-validation and WAIC for Bayesian models
 Efficient approximate leave-one-out cross-validation (LOO) for Bayesian
 models fit using Markov chain Monte Carlo. The approximation uses Pareto
 smoothed importance sampling (PSIS), a new procedure for regularizing
 importance weights. As a byproduct of the calculations, it is possible
 as well to obtain approximate standard errors for estimated predictive
 errors and for the comparison of predictive errors between models. The
 package also provides methods for using stacking and other model
 weighting techniques to average Bayesian predictive distributions.
